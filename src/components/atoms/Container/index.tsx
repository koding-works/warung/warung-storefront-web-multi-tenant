import { Container as BaseContainer, ContainerProps as BaseContainerProps } from "@chakra-ui/react"

export interface ContainerProps extends BaseContainerProps {}

export const Container = ({ children, ...props }: ContainerProps) => (
  <BaseContainer maxW={444} minH="100vh" h="100%" p={0} {...props}>
    {children}
  </BaseContainer>
)
