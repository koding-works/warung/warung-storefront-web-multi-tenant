import { Meta } from "@storybook/react"
import { Container } from "./index"

export default {
  title: "Components/Container",
  component: Container,
} as Meta

export const Basic = () => <Container>This is Mobile Container</Container>
