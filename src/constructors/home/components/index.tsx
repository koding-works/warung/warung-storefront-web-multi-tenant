/* eslint-disable no-unused-vars */
import { Box, Button, Flex, IconButton, Text } from "@chakra-ui/react"
import Link from "next/link"
import { useRouter } from "next/dist/client/router"
import useTranslation from "next-translate/useTranslation"
import { Container } from "@components/atoms"
import { Minus, Plus } from "@assets"
import { HomeSlice } from "@constructors/home/ducks/types"
import { CartSlice, Item } from "@constructors/global/cart/types"

type ComponentProps = {
  home: HomeSlice
  handler: {
    addItem: (item: Item) => void
    increaseItem: (item: Item) => void
    decreaseItem: (item: Item) => void
  }
  cart: CartSlice
  product: Item[]
}

const Component = (props: ComponentProps) => {
  const { home, handler, cart, product } = props
  console.log(product)
  const router = useRouter()
  const { t, lang } = useTranslation("home")

  const checkItem = (item: Item) => {
    const checkedItem = cart.items.find((data) => item.id === data.id)

    return checkedItem
  }

  return (
    <Container p={5}>
      <Text as="h2">{t("test")}</Text>
      <Text as="h2">{t("password")}</Text>
      {/* eslint-disable-next-line @next/next/link-passhref */}
      {/* <Link href="/" locale={router.locale === "id" ? "en" : "id"}> */}
      <Button
        my={4}
        onClick={() => router.push("/", "/", { locale: router.locale === "id" ? "en" : "id" })}
      >
        Ganti Bahasa
      </Button>
      {/* </Link> */}
      <Text as="h2">Bahasa saat ini : {lang}</Text>
      <Text as="h1" mt={4}>
        Product List
      </Text>
      {home.product.data.map((item) => (
        <Flex
          minH={90}
          shadow="md"
          my={4}
          borderRadius={10}
          p={4}
          key={item.id}
          justifyContent="space-between"
          alignItems="center"
        >
          <Box>
            <Text m={0}>{item.title}</Text>
            <Text m={0} as="p">
              {item.price}/{item.unit}
            </Text>
          </Box>
          <Flex>
            {checkItem(item) ? (
              <Box>
                <IconButton
                  icon={<Plus />}
                  aria-label="Add Item"
                  onClick={() => handler.increaseItem(item)}
                />
                <Flex alignItems="center" justifyContent="center" minH="40px" minW="40px">
                  <Text m={0}>{checkItem(item)?.total}</Text>
                </Flex>
                <IconButton
                  icon={<Minus />}
                  aria-label="Remove Item"
                  onClick={() => handler.decreaseItem(item)}
                />
              </Box>
            ) : (
              <Button onClick={() => handler.addItem(item)} disabled={item.stock === 0}>
                Add To Cart
              </Button>
            )}
          </Flex>
        </Flex>
      ))}
    </Container>
  )
}

export default Component
