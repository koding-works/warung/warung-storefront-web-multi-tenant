import { connect } from "react-redux"
import { wrapper } from "src/store/store"
import { Item } from "@constructors/global/cart/types"
import { addToCart, decreaseItem, increaseItem } from "@constructors/global/cart"
import { setCategory, setPromo, getProduct } from "./ducks"
import Component from "./components"
import { getProductList } from "../../services/product"

// @ts-ignore
export const getProps = wrapper.getServerSideProps((store) => async () => {
  const data = await getProductList()

  store.dispatch(getProduct(data))

  return { props: { data } }
})

function mapStateToProps(state: any) {
  return {
    cart: state.cart,
    home: state.home,
  }
}

function mapDispatchToProps(dispatch: any) {
  return {
    handler: {
      setCategory: (value: number) => {
        dispatch(setCategory(value))
      },
      setPromo: (value: number) => {
        dispatch(setPromo(value))
      },
      addItem: (data: Item) => {
        dispatch(addToCart(data))
      },
      increaseItem: (data: Item) => {
        dispatch(increaseItem(data))
      },
      decreaseItem: (data: Item) => {
        dispatch(decreaseItem(data))
      },
    },
  }
}

// @ts-ignore
export default connect(mapStateToProps, mapDispatchToProps)(Component)
