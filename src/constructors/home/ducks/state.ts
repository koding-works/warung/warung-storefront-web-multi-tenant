const INITIAL_STATE = {
  category: {
    data: [],
    selected: 0,
  },
  promo: {
    data: [],
    selected: 0,
  },
  product: {
    data: [],
  },
}

export default INITIAL_STATE
