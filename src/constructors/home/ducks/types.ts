export type Item = {
  id: number
  title: string
  price: number
  unit: string
  stock: number
  desc: string
  total?: number
}

export type CategoryItem = {
  id: number
  title: string
  price: number
  unit: string
  stock: number
  desc: string
  total?: number
}

export type PromoItem = {
  id: number
  title: string
}

export type HomeSlice = {
  category: {
    data: CategoryItem[]
    selected: number
  }
  promo: {
    data: PromoItem[]
    selected: number
  }
  product: {
    data: Item[]
  }
}
