import { createSlice } from "@reduxjs/toolkit"
import { HYDRATE } from "next-redux-wrapper"
import INITIAL_STATE from "./state"
import { HomeSlice } from "./types"

const slice = createSlice({
  name: "home",
  initialState: INITIAL_STATE,
  reducers: {
    getProduct: (state: HomeSlice, { payload }) => {
      state.product.data = [...payload]
    },
    getCategory: (state: HomeSlice, { payload }) => {
      state.category.data = payload
    },
    getPromo: (state: HomeSlice, { payload }) => {
      state.promo.data = payload
    },

    setCategory: (state: HomeSlice, { payload }) => {
      state.category.selected = payload
    },
    setPromo: (state: HomeSlice, { payload }) => {
      state.promo.selected = payload
    },
  },

  extraReducers: {
    [HYDRATE]: (state, { payload }) => ({
      ...state,
      ...payload.home,
    }),
  },
})

export const { getProduct, getCategory, getPromo, setCategory, setPromo } = slice.actions

export default slice.reducer
