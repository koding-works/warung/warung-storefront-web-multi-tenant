export type Item = {
  id: number
  title: string
  price: number
  unit: string
  stock: number
  desc: string
  total?: number
}

export type CartSlice = {
  items: Item[]
}
