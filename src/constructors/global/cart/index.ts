import { createSlice } from "@reduxjs/toolkit"
import INITIAL_STATE from "./state"
import { CartSlice } from "./types"

const slice = createSlice({
  name: "cart",
  initialState: INITIAL_STATE,
  reducers: {
    addToCart(state: CartSlice, { payload }) {
      const item = { ...payload, total: 1 }

      state.items = [...state.items, item]
    },
    increaseItem(state: CartSlice, { payload }) {
      const newCart = state.items.map((item) => {
        if (item.id === payload.id) {
          return {
            ...item,
            total: ((item.total as number) += 1),
          }
        }
        return item
      })

      state.items = newCart
    },
    decreaseItem(state: CartSlice, { payload }) {
      const index = state.items.findIndex((item) => item.id === payload.id)

      const newCart = () => {
        if ((state.items[index].total as number) > 1) {
          state.items[index] = {
            ...state.items[index],
            total: ((state.items[index].total as number) -= 1),
          }
        } else {
          state.items.splice(index, 1)
        }
        return state.items
      }

      newCart()
    },
  },
})

export const { addToCart, increaseItem, decreaseItem } = slice.actions

export default slice.reducer
