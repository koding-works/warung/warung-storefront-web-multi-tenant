import { PersistGate } from "redux-persist/integration/react"
import { AppProps } from "next/app"
import { ChakraProvider } from "@chakra-ui/react"
import theme from "@theme"
import { useStore } from "react-redux"
import { wrapper } from "@store/store"
import { QueryClient, QueryClientProvider } from "react-query"
import { ReactQueryDevtools } from "react-query/devtools"
import { Hydrate } from "react-query/hydration"
import { useState } from "react"

function App({ Component, pageProps }: AppProps) {
  const store = useStore()
  const [queryClient] = useState(() => new QueryClient())

  return (
    <QueryClientProvider client={queryClient}>
      <Hydrate state={pageProps.dehydratedState}>
        <PersistGate persistor={(store as any).persistor} loading={null}>
          <ChakraProvider theme={theme}>
            <Component {...pageProps} />
          </ChakraProvider>
        </PersistGate>
        <ReactQueryDevtools initialIsOpen={false} />
      </Hydrate>
    </QueryClientProvider>
  )
}

export default wrapper.withRedux(App)
