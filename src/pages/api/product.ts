import { NextApiRequest, NextApiResponse } from "next"
import ProductList from "@constants/product"

export default async (req: NextApiRequest, res: NextApiResponse) => {
  res.send(ProductList)
}
