import Axios from "axios"

export const axiosInstance = Axios.create({
  baseURL: process.env.NEXT_PUBLIC_BASE_URL,
})
