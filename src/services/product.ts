import { axiosInstance as axios } from "src/config/axios"

export const getProductList = async () => {
  const response = await axios.get("/api/product")
  return response.data
}
