import { combineReducers } from "redux"
import cartReducer from "@constructors/global/cart"
import homeReducer from "@constructors/home/ducks"

const appReducers = combineReducers({
  cart: cartReducer,
  home: homeReducer,
})

export default appReducers
