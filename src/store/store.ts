import { AnyAction, configureStore } from "@reduxjs/toolkit"
import { createLogger } from "redux-logger"
import storage from "redux-persist/lib/storage"
import { persistReducer, persistStore } from "redux-persist"
import thunk, { ThunkAction } from "redux-thunk"
import isDev from "@utils/isDev"
import { createWrapper } from "next-redux-wrapper"
import appReducers from "./reducer"

const logger = createLogger({ collapsed: true })

export const makeStore = ({ isServer }: any) => {
  if (isServer) {
    return configureStore({
      reducer: appReducers,
      middleware: (getDefaultMiddleware) =>
        getDefaultMiddleware({ serializableCheck: false }).prepend([thunk]).concat(logger),
      devTools: isDev,
    })
  } else {
    const persistConfig = {
      key: "root",
      storage,
      whitelist: ["cart"],
    }

    const persistedReducer = persistReducer(persistConfig, appReducers)

    const store = configureStore({
      reducer: persistedReducer,
      middleware: (getDefaultMiddleware) =>
        getDefaultMiddleware({ serializableCheck: false }).prepend([thunk]).concat(logger),
      devTools: isDev,
    })

    ;(store as any).persistor = persistStore(store)

    return store
  }
}

export type AppStore = ReturnType<typeof makeStore>
export type AppState = ReturnType<AppStore["getState"]>
export type AppDispatch = ReturnType<AppStore["dispatch"]>
export type AppThunkAction<ReturnType = Promise<void>> = ThunkAction<
  ReturnType,
  AppState,
  unknown,
  AnyAction
>

export const wrapper = createWrapper(makeStore)
